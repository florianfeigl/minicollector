#include "Map.h"
#include "Player.h"

#include <iostream>
#include <vector>

Player::Player() {
  playerPosition(2, 0);

  std::cout << "Object <Player> loaded." << std::endl;
}

Player::~Player() {}

void Player::getPlayer(const char player) {
  std::cout << player;
}

void Player::movePlayer(const char* direction) {
  Map mapObj;

  std::cout << "Which direction? up [w], left [a], down [s], right [d]" << std::endl << ":";

  if (direction == w) {
    (playerPosition.at(1) > mapObj.yAxis.at(0)) ? playerPosition.at(1); : std::cout << "Spring nicht über den Rand! Es gibt so viele Gründe zu leben!" << std::endl;
  } else if (direction == a) {
    (playerPosition.at(0) > mapObj.xAxis.at(0)) ? --playerPosition.at(0); : std::cout << "Du musst besoffen sein, so wie du wankst!" << std::endl;
  } else if (direction == s) {
    (playerPosition.at(1) < mapObj.yAxis.size()) ? ++playerPosition.at(1); : std::cout << "Nicht runterfallen, die Erde ist flach!" << std::endl;
  } else if (direction == d) {
    (playerPosition.at(0) < mapObj.xAxis.size()) ? ++playerPosition.at(0); : std::cout << "Brumm, brumm! Leider geil, aber dumm!" << std::endl;
  } else {
    std::cout << "Sorry, I don't know what to do?" << std::endl;
}