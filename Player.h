#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

class Player {

  public:
  Player();
  ~Player();
  void getPlayer(const char);
  void movePlayer(const char*);

  const char player[4] = "<>|";
  std::vector<int> playerPosition(int, int);
  int x, y;
  const char* direction;
  const char* w = "w";
  const char* a = "a";
  const char* s = "s";
  const char* d = "d";
};
#endif