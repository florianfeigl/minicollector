#ifndef MAP_H
#define MAP_H

#include <vector>

class Map {

  public:
  Map();
  ~Map();
  void createMap();
  void drawMap();

  int yin, xin;
  std::vector<int> yAxis;
  std::vector<int> xAxis;

};
#endif