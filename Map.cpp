#include "Map.h"
#include "Player.h"

#include <iostream>
#include <vector>

Map::Map() {
  yAxis = {8};
  xAxis = {8};

  std::cout << "Object <Map> loaded." << std::endl;
}

Map::~Map() {}

void Map::createMap() {

  std::cout << "Length of Y axis: ";
  std::cin >> yin;
  std::cout << "Length of X axis: ";
  std::cin >> xin;

  yAxis = {yin};
  xAxis = {xin};

  for (unsigned long y = 0; y < yAxis.size(); ++y) {
    yAxis.at(y) = y;
  }
  for (unsigned long x = 0; x < xAxis.size(); ++x) {
    xAxis.at(x) = x;
  }
  std::printf("Successfully created a field with %u in height and %u in width", yin, xin);
}


void Map::drawMap() {
  Player playerObj;
  std::cout << "*************************" << std::endl;
  for (unsigned long y=0; y < yAxis.size(); ++y) {
    std::cout << "|";  // If a new line is starting, start with "|"

    for (unsigned long x=0; x < xAxis.size(); ++x) {
      if (xAxis.at(x) == playerObj.position.at(0) && yAxis.at(y) == playerObj.position.at(1)) {
        std::cout << playerObj.player;
      } else {
        std::cout << "__|";
    }
  }
    std::cout << std::endl;
  }
  std::cout << "*************************" << std::endl;
}